# HTML5 Video Converter

## Description
Sometimes we have to convert videos for embedded videos
`ffmpeg` is a universal converter program, we can create the `mp4` and `webm` files with the correct settings

## Url(s)
ffmpeg: https://www.ffmpeg.org/

## Install
- extract to zip into a directory the path shouldn't contain spaces or special characters
- create an icon on the desktop for this file: `ffmpeg\bin\conv.bat`

## Optional
you can add the `ffmpeg\bin\` directory to your PATH variable  
after that you can call the command like this:
`conv.bat videofilename`

## How to use the desktop icon
- drag and drop the video file into your icon, it will start the conversion
- in this case the converted mp4 and webm files will be created next to the `conv.bat` file
- the file's path and name shouldn't contain special characters or spaces

## ffmpeg commands
These 2 commands executed in the `conv.bat` file.  
You can change them or you can use  
`ffmpeg` command directly in the command line,  
if you added the `ffmpeg\bin\` directory to you PATH variable
(`%1` the input file and `%~n1-converted` the output)  

`ffmpeg -i %1 -f webm -vcodec libvpx -acodec libvorbis -ab 128000 -crf 5 -b:v 1M -movflags +faststart %~n1-converted.webm`  

`ffmpeg -i %1 -acodec aac -strict experimental -ac 2 -ab 128k -vcodec libx264 -preset slow -f mp4 -crf 22 -movflags +faststart %~n1-converted.mp4`
